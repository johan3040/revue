import { test, expect } from '@playwright/test';

// See here how to get started:
// https://playwright.dev/docs/intro
test('visits the app root url', async ({ page }) => {
  await page.goto('/');
  await page.getByLabel('Din mail').type('test@test.com')
  await page.getByLabel('Ditt lösenord').type('Test123')
  await page.getByRole('button', {name: 'Skicka'}).click()

  await expect(page.getByRole('button', {name: 'Logga ut'})).toBeVisible()
})
